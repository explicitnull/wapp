package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
)

const (
	cookieExpMinutes = 60
)

var hashKey = []byte("secret")
var blockKey = []byte("1234567890123456")
var sc = securecookie.New(hashKey, blockKey)

func main() {
	registerHttp()
	// if err := http.ListenAndServeTLS(":443", Opts.TlsFullChain, Opts.TlsPrivKey, nil); err != nil {
	// 	log.Fatalf("ListenAndServeTLS error: %v\n", err)
	// }
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("ListenAndServe error: %v\n", err)
	}
}

func registerHttp() {
	mx := mux.NewRouter()
	http.Handle("/", mx)

	// mx.HandleFunc("/", homeHandler)
	mx.HandleFunc("/set/", setCookieHandler)
	mx.HandleFunc("/read/", readCookieHandler)
	// mx.HandleFunc("/login/", loginHandler)
	// mx.HandleFunc("/logout/", logoutHandler)
}

func setCookieHandler(w http.ResponseWriter, r *http.Request) {
	value := map[string]string{
		"name": setCookieAction(),
	}
	expiration := time.Now().Add(cookieExpMinutes * time.Minute)

	if encoded, err := sc.Encode("username", value); err == nil {
		cookie := &http.Cookie{
			Name:  "username",
			Value: encoded,
			Path:  "/",
			// Secure:   true,
			HttpOnly: true,
			Expires:  expiration,
		}

		http.SetCookie(w, cookie)
	}

	fmt.Fprintf(w, "Куку засэтили, сэр")
}

func setCookieAction() string {
	return "testuser"
}

func readCookieHandler(w http.ResponseWriter, r *http.Request) {
	if cookie, err := r.Cookie("username"); err == nil {
		value := make(map[string]string)

		if err = sc.Decode("username", cookie.Value, &value); err == nil {
			usr := value["name"]
			fmt.Fprintf(w, "Сэр, вы - %q", usr)
		}
	} else if err == http.ErrNoCookie {
		fmt.Fprintf(w, "Сэр, нет куки")
	} else {
		fmt.Fprintf(w, "%v", err)
	}
}
